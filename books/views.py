from django.views import generic as generic_views
from . import models,settings
from models import Books,BookCategory
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext

from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BOOK CATEGORY LIST~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class BookCategoryListView(generic_views.ListView):
    model=BookCategory
    # paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name='books/category_list.html'

    def get_context_data(self, **kwargs):
        context = super(BookCategoryListView, self).get_context_data(**kwargs)
        context['category'] = BookCategory.objects.all()
        return context
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~CATEGORY BOOKS LIST~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class BooksListView(generic_views.ListView):
    model = Books
    paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name = 'books/category_books.html'


    def get_context_data(self, **kwargs):
        context = super(BooksListView, self).get_context_data(**kwargs)
        # if 'category' in self.kwargs:
        cat_obj=BookCategory.objects.get(slug=self.kwargs['category'])
        books_obj = Books.objects.filter(category=cat_obj.id)
        # context['book'] = books_obj


        items =books_obj
        paginator = Paginator(items, 20)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(self.request.GET.get('page', '1'))
        except ValueError:
            page = 1

        # If page request (9999) is out of range, deliver last page of results.
        try:
            pages = paginator.page(page)
        except (EmptyPage, InvalidPage):
            pages = paginator.page(paginator.num_pages)

        page_count = len(pages.paginator.page_range)
        current_page = pages.number
        page_list = []

        DISPLAY_PAGES_COUNT = 9
        MIN_COUNT = 5

        if page_count <= DISPLAY_PAGES_COUNT:
            page_list = range(1, page_count + 1)
        elif page_count == 10:
            if current_page < MIN_COUNT:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list.append(page_count-1)
                page_list.append(page_count)
            elif current_page >MIN_COUNT:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (MIN_COUNT + 1):
                    page_list += range(MIN_COUNT, page_count+1)
                else:
                    page_list += range(MIN_COUNT+1, page_count+1)
            else:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page+3)
                page_list.append(-1)
                page_list.append(page_count)
        else:
            if current_page < MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 2)
                page_list.append(-1)
                page_list +=range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page > MIN_COUNT and current_page <= (page_count - MIN_COUNT):
                page_list = [1,2,-1]
                page_list += range(current_page-2, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            else:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (page_count - MIN_COUNT + 1):
                    page_list += range(page_count - MIN_COUNT, page_count+1)
                else:
                    page_list += range(page_count - MIN_COUNT + 1, page_count+1)


        context = super(BooksListView, self).get_context_data(**kwargs)
        context['book'] = Books.objects.all()
        context['pages'] = pages
        context['page_list'] = page_list
        context['category']=cat_obj
        return context


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BOOK DETAIL~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BookDetailView(generic_views.ListView):
    model = Books
    template_name = 'books/book_detail.html'

    def get_context_data(self, **kwargs):
        context = super(BookDetailView, self).get_context_data(**kwargs)
        context['book'] = Books.objects.all()
        book_obj=Books.objects.get(slug=self.kwargs['book'])
        context['book'] = book_obj
        return context
        #
        # if 'category' in self.kwargs:
        #     cat_obj=BookCategory.objects.get(slug=self.kwargs['category'])
        #     books_obj = Books.objects.filter(category=cat_obj.id)
        # context = super(BookDetailView, self).get_context_data(**kwargs)
        # context['book'] = Books.objects.all()
        # # context['category']=BookCategory.objects.all()
        # return context
