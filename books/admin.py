from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin
from models import Books,BookCategory

from books.forms import BooksForm,BooksCategoryForm

class BooksAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    list_display = ('name', 'category','author')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
    form = BooksForm

class BooksCategoryAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    # list_display = ('name', 'author')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
    form = BooksCategoryForm


admin.site.register(Books, BooksAdmin)
admin.site.register(BookCategory,BooksCategoryAdmin)
# Register your models here.
