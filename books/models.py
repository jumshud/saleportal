from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.db import models

# from django.core.validators import MaxLengthValidator

# Create your models here.
class BookCategory(models.Model):
    name=models.CharField(('Name'),max_length=255)
    slug = models.SlugField(_('Slug'),
                        help_text=_('A slug is a short name which uniquely identifies the services item for this day'))


    def __unicode__(self):
        return self.name


class Books(models.Model):

    image = models.ImageField(_('image'), upload_to='books/')

    name = models.CharField(_('Name'), max_length=255)
    slug = models.SlugField(_('Slug'),
                        help_text=_('A slug is a short name which uniquely identifies the services item for this day'))

    author=models.CharField(_('Author(By)'),max_length=255)
    publisher=models.CharField(_('Publisher'),max_length=255)
    # {#fildin adini deyiw#}
    published=models.CharField(_('Published'),max_length=255,
                                    help_text=_('Ae bunu duz yaz!!'))
    page_count=models.CharField(_('Pages Count'),max_length=255)

    format=models.CharField(_('Format'),max_length=255,
                                    help_text=_("This is book's paper format"))
    description=models.TextField(_('Description'))
    category = models.ForeignKey(BookCategory, name="category")
    isbn=models.CharField(_('ISBN'),max_length=255)
    link = models.CharField(_('LINK'),max_length=255)

    class Meta:
        verbose_name = _('books')
        verbose_name_plural = _('Books')
        ordering = ('description', )


   # def choose_category(self):
   #
   #     return self.name
