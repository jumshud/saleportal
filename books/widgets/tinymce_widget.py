from django.conf import settings
from django.utils.translation import get_language
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.encoding import smart_unicode
from django.utils import simplejson
from django.template.defaultfilters import escape
from django.forms.widgets import flatatt

from tinymce.widgets import TinyMCE, get_language_config
import tinymce.settings

#import cms.plugins.text.settings
#from cms.utils import cms_static_url


class TinyMCEEditor(TinyMCE):

    def __init__(self, installed_plugins=None, **kwargs):
        super(TinyMCEEditor, self).__init__(**kwargs)
        self.installed_plugins = installed_plugins

    def render_additions(self, name, value, attrs=None):
        language = get_language()
        context = {
            'name': name,
            'language': language,
            'CMS_MEDIA_URL': settings.STATIC_URL,
            'installed_plugins': self.installed_plugins,
        }
        return mark_safe(render_to_string(
            'cms_links/widgets/tinymce.html', context))



    
