from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
# from django.conf import settings
from . import models
from . import views

urlpatterns = patterns('django.views.generic.date_based',
    url(r'^(?P<category>.*?)/(?P<book>.*?)/$',
            views.BookDetailView.as_view(), name='book_detail'),

    url(r'^(?P<category>.*?)/$',
            views.BooksListView.as_view(), name='category_books'
       ),

    url(r'^$',views.BookCategoryListView.as_view(paginate_by=20), name='category_list'
       ),





)