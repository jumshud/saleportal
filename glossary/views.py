import string
from django.views import generic as generic_views
from django.views.generic.list import ListView
from django.db import connection, transaction
from glossary.models import Term, Subject
from django import template
from collections import OrderedDict

class GlossarySubjectView(ListView):
    """
    Return a list of all terms

    """
    model = Subject
    template_name = 'glossary/glossary_subject.html'    

    def get_context_data(self, **kwargs):
        #
        subjects = Subject.objects.all()
        context = super(GlossarySubjectView, self).get_context_data(**kwargs)    
        context['subjects'] = subjects    
        
        return context


class ServiceTermView(ListView):
    """
    Return a list of all terms

    """
    model = Term
    template_name = 'glossary/glossary_subject_term.html'    

    def get_context_data(self, **kwargs):
        subject_obj=Subject.objects.get(slug=self.kwargs['slug'])
        subj_terms = Term.objects.filter(subject = subject_obj.id)
        GLOSSARY_FILTERS = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

        glossaries = {}
        glossaries1 = {}

        if subj_terms:
            for letter in GLOSSARY_FILTERS:
                term_list =[]
                for term in subj_terms:
                    if  term.title.upper().startswith(letter):
                        term_list.append(term.title)
                        term_list.append(term.description)
                        
                        glossaries.update({letter:term_list})
        glossaries = OrderedDict(sorted(glossaries.items(), key=lambda t: t[0]))


                    
        

        context = super(ServiceTermView, self).get_context_data(**kwargs)  

        
        context['GLOSSARY_FILTERS'] = GLOSSARY_FILTERS

        # for letter,term_list in sorted(glossaries.split(),letter=str.upper()):
        context['glossaries'] = glossaries


        context['subject']= subject_obj.name

        return context