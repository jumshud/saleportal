from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _


class Subject(models.Model):
    name = models.CharField(max_length = 150)
    slug = models.SlugField(unique = True)
    description = models.TextField()

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')
        ordering = ('-name', )


    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('glossary:glossary_subject_terms', kwargs={
                                    'slug': self.slug})




class Term(models.Model):
    subject = models.ForeignKey(Subject, related_name='Subject')
    title = models.CharField(max_length=250)
    description = models.TextField()

    class Meta:
        ordering = ['title']
    def __unicode__(self):
        return self.title

    

