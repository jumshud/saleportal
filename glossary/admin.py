from django.contrib import admin
from glossary.models import Term, Subject

class SubjectAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name']}
    list_display = ('name','description')
    search_fields = ('name', 'description')

admin.site.register(Subject, SubjectAdmin)


class TermAdmin(admin.ModelAdmin):
    list_display = ('subject','title','description')
    search_fields = ('title', 'subject')

admin.site.register(Term, TermAdmin)