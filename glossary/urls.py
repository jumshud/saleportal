from django.conf.urls import patterns, include, url
from glossary.models import Term
from . import views
from django.views.generic import DetailView

terms = Term.objects.all()

urlpatterns = patterns('',
    url(r'^$',   views.GlossarySubjectView.as_view(), name='glossary_subject_list'),
    url(r'^(?P<slug>[-\w]+)/$',
         views.ServiceTermView.as_view(), name='glossary_subject_terms'),
)
