import random
from django import template
from django.db.models import get_model

register = template.Library()

#Subject = get_model('glossary', 'Subject')

@register.filter()
def get_list_title(list, index):
    if int(index)%2 == 0:
        return list[index]
    return u""

@register.filter()
def get_list_desc(list, index):
    if int(index)%2 != 0:
        return list[index]
    return u""


@register.filter()
def get_item(dict, key):
    return dict.get(key)



class IncrementVarNode(template.Node):

    def __init__(self, var_name):
        self.var_name = var_name

    def render(self,context):
        value = context[self.var_name]
        context[self.var_name] = value + 1
        return u""

def increment_var(parser, token):

    parts = token.split_contents()
    if len(parts) < 2:
        raise template.TemplateSyntaxError("'increment' tag must be of the form:  {% increment <var_name> %}")
    return IncrementVarNode(parts[1])

register.tag('increment', increment_var)