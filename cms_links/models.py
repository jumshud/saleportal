from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.db import models

# from django.core.validators import MaxLengthValidator

# Create your models here.

class Links(models.Model):

    title = models.CharField(_('Title'), max_length=255)

    link=models.CharField('Link',max_length=255)

    image = models.ImageField('image', upload_to='links/')

    description=models.TextField(_('Description'))

    class Meta:
        verbose_name = _('links')
        verbose_name_plural = _('Links')
        ordering = ('description', )

