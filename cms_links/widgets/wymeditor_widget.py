from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from django.forms import Textarea

from django.conf import settings
#from cms.utils import cms_static_url
#from cms.plugins.text import settings as text_settings
from django.utils.translation.trans_real import get_language


class WYMEditor(Textarea):

        

    def __init__(self, attrs=None, installed_plugins=None):
        """
        Create a widget for editing text + plugins.

        installed_plugins is a list of plugins to display that are text_enabled
        """
        self.attrs = {'class': 'wymeditor'}
        if attrs:
            self.attrs.update(attrs)
        super(WYMEditor, self).__init__(attrs)
        self.installed_plugins = installed_plugins

    def render_textarea(self, name, value, attrs=None):
        return super(WYMEditor, self).render(name, value, attrs)

    

    def render(self, name, value, attrs=None):
        return self.render_textarea(name, value, attrs) + \
            self.render_additions(name, value, attrs)
