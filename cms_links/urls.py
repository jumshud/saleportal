from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
# from django.conf import settings
from . import models
from . import views

urlpatterns = patterns('django.views.generic.date_based',
        url(r'^$',views.LinksListView.as_view(paginate_by=10), name='links_list'
        )
)