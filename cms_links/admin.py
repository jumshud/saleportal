from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin
from cms_links.models import Links

from cms_links.forms import LinksForm


class LinksAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    list_display = ('title', 'link')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    # prepopulated_fields = {'link': ('title',)}
    form = LinksForm



admin.site.register(Links, LinksAdmin)

# Register your models here.
