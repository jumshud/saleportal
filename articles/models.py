try:
    from django.utils.timezone import now as datetime_now
    datetime_now  # workaround for pyflakes
except ImportError:
    from datetime import datetime
    datetime_now = datetime.now

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
# from django.db import models
from cms.models import CMSPlugin
from . import settings


class ArticleCategory(models.Model):
        name = models.CharField(_('Name'),max_length=255)
        slug = models.SlugField(_('Slug'),
                        help_text=_('A slug is a short name which uniquely identifies the services item for this day'))


        def __unicode__(self):
            return self.name

class Articles(models.Model):
        image = models.ImageField('image', upload_to='articles/')
        name = models.CharField(_('Name'),max_length=255)
        slug = models.SlugField(_('Slug'),
                        help_text=_('A slug is a short name which uniquely identifies the services item for this day'))
        author=models.CharField(_('Author(By)'),max_length=255)
        pub_date = models.DateField(_('Publication date'), default=datetime_now,db_index=True)

        source = models.CharField(_('Source'),max_length=255,
                                  help_text=_('NOTE:: Write with HTTP!!'))
        text = models.TextField(_('Text'),)
        category = models.ForeignKey(ArticleCategory, name="category")


        class Meta:
            verbose_name = _('Articles')
            verbose_name_plural = _('Articles')
            ordering = ('-pub_date', )
