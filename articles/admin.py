from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin
from models import Articles,ArticleCategory

from articles.forms import ArticlesForm,ArticleCategoryForm

class ArticlesAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    list_display = ('name', 'author')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
    form = ArticlesForm

class ArticleCategoryAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    list_display = ('name', 'slug')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
    form = ArticleCategoryForm




admin.site.register(Articles, ArticlesAdmin)
admin.site.register(ArticleCategory,ArticleCategoryAdmin)
# Register your models here.
