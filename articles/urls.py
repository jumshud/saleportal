from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
# from django.conf import settings
from . import models
from . import views

urlpatterns = patterns('django.views.generic.date_based',
    url(r'^(?P<category>.*?)/(?P<article>.*?)/$',
            views.ArticleDetailView.as_view(), name='article_detail'),

    url(r'^(?P<category>.*?)/$',
            views.ArticlesListView.as_view(paginate_by=10), name='category_articles'
       ),

    url(r'^$',views.ArticleCategoryListView.as_view(), name='category_list'
       ),





)