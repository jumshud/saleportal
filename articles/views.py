from django.views import generic as generic_views
from . import models,settings
from models import Articles,ArticleCategory
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext

from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django.shortcuts import render

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Categories View~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ArticleCategoryListView(generic_views.ListView):

    model=ArticleCategory
    # paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name='articles/category_list.html'
    articless= {}

    def get_context_data(self, **kwargs):
        context = super(ArticleCategoryListView, self).get_context_data(**kwargs)
        # articles_all = Articles.objects.all()
        # context['categories']=ArticleCategory.objects.all()
        categories_all = ArticleCategory.objects.all()
        # article=Articles.objects.all()
        # for category in context['category']:
            # cat_obj=ArticleCategory.objects.get(slug=context['category'])
            # artic_obj=Articles.objects.filter(category_id=cat_obj.id)
        categories_dict = {}
        articles_dict = {}
        for cat in categories_all:
            categories_dict.update({cat.id:[cat.id, cat.slug, cat.name]})

            categories_articles = Articles.objects.filter(category=cat.id).order_by('pub_date')[:3]

            for article in categories_articles:
                articles_dict.update({article.id:[cat.id, article.name, article.slug]})

            # if name == Articles('category'):
            #             articles_obj = Articles.objects.order_by('pub_date')[:3]
            #             context['article']=articles_obj


                # return context
        context['categories'] = categories_dict
        context['articles'] = articles_dict
        return context

#     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Articles in Category~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ArticlesListView(generic_views.ListView):
    model = Articles
    paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name = 'articles/category_articles.html'


    def get_context_data(self, **kwargs):
        context = super(ArticlesListView, self).get_context_data(**kwargs)
        # if 'category' in self.kwargs:
        cat_obj=ArticleCategory.objects.get(slug=self.kwargs['category'])
        articles_obj = Articles.objects.filter(category=cat_obj.id)
        # context['book'] = books_obj


        items =articles_obj
        paginator = Paginator(items, 10)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(self.request.GET.get('page', '1'))
        except ValueError:
            page = 1

        # If page request (9999) is out of range, deliver last page of results.
        try:
            pages = paginator.page(page)
        except (EmptyPage, InvalidPage):
            pages = paginator.page(paginator.num_pages)

        page_count = len(pages.paginator.page_range)
        current_page = pages.number
        page_list = []

        DISPLAY_PAGES_COUNT = 9
        MIN_COUNT = 5

        if page_count <= DISPLAY_PAGES_COUNT:
            page_list = range(1, page_count + 1)
        elif page_count == 10:
            if current_page < MIN_COUNT:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list.append(page_count-1)
                page_list.append(page_count)
            elif current_page >MIN_COUNT:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (MIN_COUNT + 1):
                    page_list += range(MIN_COUNT, page_count+1)
                else:
                    page_list += range(MIN_COUNT+1, page_count+1)
            else:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page+3)
                page_list.append(-1)
                page_list.append(page_count)
        else:
            if current_page < MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 2)
                page_list.append(-1)
                page_list +=range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page > MIN_COUNT and current_page <= (page_count - MIN_COUNT):
                page_list = [1,2,-1]
                page_list += range(current_page-2, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            else:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (page_count - MIN_COUNT + 1):
                    page_list += range(page_count - MIN_COUNT, page_count+1)
                else:
                    page_list += range(page_count - MIN_COUNT + 1, page_count+1)


        context = super(ArticlesListView, self).get_context_data(**kwargs)
        context['article'] = Articles.objects.all()
        context['pages'] = pages
        context['page_list'] = page_list
        context['category']=cat_obj
        context['page_count'] = page_count
        return context


# Article
# class ArticlesListView(generic_views.ListView):
#     model=Articles
#     template_name='articles/category_articles.html'
#
#     def get_context_data(self,**kwargs):
#         context=super(ArticlesListView,self).get_context_data(**kwargs)
#         context['category']=Articles.objects.all()
#         return context


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Article Detail~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ArticleDetailView(generic_views.ListView):
        model=Articles
        template_name='articles/article_detail.html'

        def get_context_data(self, **kwargs):
            context = super(ArticleDetailView, self).get_context_data(**kwargs)
            context['article'] = Articles.objects.all()
            article_obj=Articles.objects.get(slug=self.kwargs['article'])
            context['article'] = article_obj
            return context