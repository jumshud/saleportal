from django.views import generic as generic_views
from django.shortcuts import render,render_to_response
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext

from . import models
from models import News
from settings import ARCHIVE_PAGE_SIZE
from django.shortcuts import render,render_to_response


class PublishedNewsMixin(object):
    """
    Since the queryset also has to filter elements by timestamp
    we have to fetch it dynamically.
    """
    def get_queryset(self):
        return models.News.published.all()


class ArchiveIndexView(PublishedNewsMixin, generic_views.ListView):
    """
    A simple archive view that exposes following context:

    * latest
    * date_list
    * paginator
    * page_obj
    * object_list
    * is_paginated

    The first two are intended to mimic the behaviour of the
    date_based.archive_index view while the latter ones are provided by
    ListView.
    """
    template_name = 'cmsplugin_news/news_archive.html'
    include_yearlist = True
    date_field = 'pub_date'

    def get_context_data(self, **kwargs):
        items = self.get_queryset()
        paginator = Paginator(items, ARCHIVE_PAGE_SIZE)
        # Make sure page request is an int. If not, deliver first page.
        try:
            page = int(self.request.GET.get('page', '1'))
        except ValueError:
            page = 1

        # If page request (9999) is out of range, deliver last page of results.
        try:
            pages = paginator.page(page)
        except (EmptyPage, InvalidPage):
            pages = paginator.page(paginator.num_pages) 

        page_count = len(pages.paginator.page_range)
        current_page = pages.number
        page_list = []
         
        DISPLAY_PAGES_COUNT = 9
        MIN_COUNT = 5

        if page_count <= DISPLAY_PAGES_COUNT:
            page_list = range(1, page_count + 1)
        elif page_count == 10:
            if current_page < MIN_COUNT:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list.append(page_count-1)
                page_list.append(page_count)
            elif current_page >MIN_COUNT:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (MIN_COUNT + 1):
                    page_list += range(MIN_COUNT, page_count+1)
                else:
                    page_list += range(MIN_COUNT+1, page_count+1)
            else:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page+3)
                page_list.append(-1)
                page_list.append(page_count)
        else:
            if current_page < MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 1)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT-1:
                page_list= range(1, MIN_COUNT + 2)
                page_list.append(-1)
                page_list +=range(page_count-1, page_count + 1)
            elif current_page == MIN_COUNT:
                page_list = [1,2,-1]
                page_list += range(current_page-1, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            elif current_page > MIN_COUNT and current_page <= (page_count - MIN_COUNT):
                page_list = [1,2,-1]
                page_list += range(current_page-2, current_page + 3)
                page_list.append(-1)
                page_list += range(page_count-1, page_count + 1)
            else:
                page_list = [1,2]
                page_list.append(-1)
                if current_page == (page_count - MIN_COUNT + 1):
                    page_list += range(page_count - MIN_COUNT, page_count+1)
                else:
                    page_list += range(page_count - MIN_COUNT + 1, page_count+1)
                



                

        # elif current_page < MIN_COUNT:
        #     a = range(1, DISPLAY_PAGES_COUNT+1)
        


        context = super(ArchiveIndexView, self).get_context_data(**kwargs)
        if self.include_yearlist:
            date_list = self.get_queryset().dates('pub_date', 'year')[::-1]
            context['pages'] = pages
            context['page_list'] = page_list
            context['page_count'] = page_count
        return context



class DetailView(PublishedNewsMixin, generic_views.DateDetailView):
    month_format = '%m'
    date_field = 'pub_date'


class MonthArchiveView(PublishedNewsMixin, generic_views.MonthArchiveView):
    month_format = '%m'
    date_field = 'pub_date'


class YearArchiveView(PublishedNewsMixin, generic_views.YearArchiveView):
    month_format = '%m'
    date_field = 'pub_date'


class DayArchiveView(PublishedNewsMixin, generic_views.DayArchiveView):
    month_format = '%m'
    date_field = 'pub_date'
	
