# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

from . import feeds
from . import views
from cmsplugin_news.settings import ARCHIVE_PAGE_SIZE




urlpatterns = patterns('django.views.generic.date_based',
    url(r'^$', #paginate_by must be egual 
        views.ArchiveIndexView.as_view(paginate_by=ARCHIVE_PAGE_SIZE), name='news_archive_index'),

    url(r'^(?P<year>\d{4})/$',
        views.YearArchiveView.as_view(), name='news_archive_year'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
        views.MonthArchiveView.as_view(), name='news_archive_month'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
        views.DayArchiveView.as_view(), name='news_archive_day'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        views.DetailView.as_view(), name='news_detail'),

    url(r'^feed/$', feeds.NewsFeed()),
)
