from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.db import models
# Create your models here.

class Feedback(models.Model):

    email = models.EmailField(_('Your email address '))
    fullname = models.CharField(_('Your Name'),max_length = 255)
    subject = models.CharField(_('Subject'),max_length =255)
    text = models.TextField(_('Message'))