from django.shortcuts import render
from django.views import generic as generic_views
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext

from django.shortcuts import render,render_to_response,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render
from forms import FeedbackForm
from django.views.generic.edit import UpdateView
# Create your views here.


def feedback(request ):
        """do something with a form"""
        form = FeedbackForm(request.POST or None)
        # programs_area=PartnershipArea.objects.all()
            # make the form

        if form.is_valid():
               obj = form.save()
               obj.save()

               # return redirect('/')
               return render_to_response('feedback/feedback_sent.html',context_instance=RequestContext(request))


        return render_to_response('feedback/feedback.html',
                                  {'form': form},
                                  context_instance=RequestContext(request)
                              )