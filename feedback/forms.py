from __future__ import absolute_import
from django import forms
from django.conf import settings

from cms.plugin_pool import plugin_pool
# from cms.plugins.text.settings import USE_TINYMCE

# from .widgets.wymeditor_widget import WYMEditor
from feedback.models import Feedback


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        # field=['text']
    # def _get_widget(self):
    #     plugins = plugin_pool.get_text_enabled_plugins(placeholder=None,
    #             page=None)
    #     if  "tinymce" in settings.INSTALLED_APPS:
    #         from cms_links.widgets.tinymce_widget import TinyMCEEditor
    #         return TinyMCEEditor(installed_plugins=plugins)
    #     else:
    #         return WYMEditor(installed_plugins=plugins)
