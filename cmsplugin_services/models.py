try:
    from django.utils.timezone import now as datetime_now
    datetime_now  # workaround for pyflakes
except ImportError:
    from datetime import datetime
    datetime_now = datetime.now

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from cms.models import CMSPlugin

from . import settings


class ServiceCategory(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    slug = models.SlugField(_('Slug'),
                            help_text=_(
                                'A slug is a short name which uniquely identifies the services item for this day'))

    description = models.TextField(_('Description'), blank=True)


    def __unicode__(self):
        return self.name


class Service(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'),
                            help_text=_(
                                'A slug is a short name which uniquely identifies the services item for this day'))
    content = models.TextField(_('Content'), blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    category = models.ForeignKey(ServiceCategory, name="category")
    model_name = models.CharField(_('Model'), max_length = 50, blank=True, null=True)
    objects = models.Manager()

    class Meta:
        verbose_name = _('services')
        verbose_name_plural = _('services')
        ordering = ('-created', )

    def __unicode__(self):
        return self.title