# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings
from . import models
from . import views

urlpatterns = patterns('django.views.generic.date_based',

    url(r'^$', views.ServiceListView.as_view(), name='service_list'),

    url(r'^(?P<category>.*?)-category/$',
        views.ServiceListView.as_view(), name='service_list_category'),

    url(r'^(.*?)-category/(?P<service>.*?)/$',
        views.ServiceDetailView.as_view(), name='service_detail'),

)
