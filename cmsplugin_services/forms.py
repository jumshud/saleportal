from __future__ import absolute_import
from django import forms
from django.conf import settings

from cms.plugin_pool import plugin_pool
#from cms.plugins.text.settings import USE_TINYMCE

from .widgets.wymeditor_widget import WYMEditor
from .models import Service,ServiceCategory
# from ckeditor.widgets import CKEditorWidget

class ServicesForm(forms.ModelForm):
    class Meta:
        model = Service

    def _get_widget(self):
        plugins = plugin_pool.get_text_enabled_plugins(placeholder=None,
                page=None)
        if  "tinymce" in settings.INSTALLED_APPS:
            from cmsplugin_services.widgets.tinymce_widget import TinyMCEEditor
            return TinyMCEEditor(installed_plugins=plugins)
        else:
            return WYMEditor(installed_plugins=plugins)

    def __init__(self, *args, **kwargs):
        super(ServicesForm, self).__init__(*args, **kwargs)
        widget = self._get_widget()
        self.fields['content'].widget = widget

class ServiceCategoryForm(forms.ModelForm):
    # content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = ServiceCategory
        # widgets = {
        #     'name': CKEditorWidget(editor_options={'startupFocus': True})
        # }
        # field=['content']
    # def _get_widget(self):
    #     plugins = plugin_pool.get_text_enabled_plugins(placeholder=None,
    #             page=None)
    #     if  "tinymce" in settings.INSTALLED_APPS:
    #         from cms_links.widgets.tinymce_widget import TinyMCEEditor
    #         return TinyMCEEditor(installed_plugins=plugins)
    #     else:
    #         return WYMEditor(installed_plugins=plugins)
    #
