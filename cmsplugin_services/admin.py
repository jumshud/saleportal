from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin

from cmsplugin_services.forms import ServicesForm,ServiceCategoryForm
from cmsplugin_services.models import Service
from cmsplugin_services.models import ServiceCategory

# from django.contrib import admin
# from django.db import models
# from django import forms
#
# from .models import Entry
#
# class EntryAdmin(admin.ModelAdmin):
#     formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
#     list_display = ['title', 'creation_date', 'status']
#
#     class Media:
#         js = ('ckeditor/ckeditor.js',)
#
# admin.site.register(Entry, EntryAdmin)

class ServiceCategoryAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    # date_hierarchy = 'pub_date'
    # list_display = ('name')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    # search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('name',)}
    form = ServiceCategoryForm

    class Media:
        js = ('ckeditor/ckeditor.js',)

class ServicesAdmin(admin.ModelAdmin):
    """
        Admin for services
    """
    date_hierarchy = 'created'
    list_display = ('slug', 'title','created')
    #list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}
    form = ServicesForm

    # actions = ['make_published', 'make_unpublished']

    save_as = True
    save_on_top = True

    def queryset(self, request):
        """
            Override to use the objects and not just the default visibles only.
        """
        return Service.objects.all()
    class Media:
        js = ('ckeditor/ckeditor.js',)


admin.site.register(Service, ServicesAdmin)
admin.site.register(ServiceCategory,ServiceCategoryAdmin)
