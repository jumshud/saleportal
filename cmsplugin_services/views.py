from django.views import generic as generic_views

from . import models,settings
from models import Service,ServiceCategory
from django.shortcuts import render,render_to_response
from django.http import HttpResponse

class ServiceListView(generic_views.ListView):
    model = Service
    template_name = 'cmsplugin_services/service_list.html'

    def get_context_data(self, **kwargs):
        context = super(ServiceListView, self).get_context_data(**kwargs)
        context['category'] = ServiceCategory.objects.all()
        if 'category' in self.kwargs:
            cat_obj=ServiceCategory.objects.get(slug=self.kwargs['category'])
            services_obj = Service.objects.filter(category=cat_obj.id)
            context['from_category'] = "true"
            context['selected_category'] = cat_obj.name
        else:
            services_obj = Service.objects.all()
            context['from_category'] = ""
        context['service'] = services_obj
        return context


class ServiceDetailView(generic_views.ListView):
    model = Service
    template_name = 'cmsplugin_services/service_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ServiceDetailView, self).get_context_data(**kwargs)
        context['service'] = Service.objects.all()
        service_obj=Service.objects.get(slug=self.kwargs['service'])
        context['service']=service_obj
        context['category']=ServiceCategory.objects.all()
        return context

