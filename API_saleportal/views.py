from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from sale_order.models import Message, Order, Offer, OrderStage,OrderStageFile, OrderStageFileComment
from user_profile.models import User
from rest_framework.decorators import api_view
import json
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime


@api_view(['POST'])
@csrf_exempt
def create_message(request):
    if request.method == 'POST':
        order_id = int(request.POST['order_id'])

        try:
            order_obj = Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            json_data = json.dumps({'message':'Order object not found for this id'}, cls=DjangoJSONEncoder)
            return HttpResponse(json_data, content_type='application/json')

        #Get customer object
        to_user_id = int(request.POST['to_user_id'])
        try:
            user = User.objects.get(id=to_user_id)
        except User.DoesNotExist:
            json_data = json.dumps({'message':'User object not found for this id'}, cls=DjangoJSONEncoder)
            return HttpResponse(json_data, content_type='application/json')

        new_mes_obj = Message()
        new_mes_obj.orderID = order_obj
        new_mes_obj.to_user = user
        new_mes_obj.message = request.POST['message']
        new_mes_obj.subject = request.POST['subject']
        new_mes_obj.save()
        json_data = json.dumps({'message':'success'}, cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type='application/json')


@csrf_exempt
def import_comment(request):
    if request.method == 'POST':
        stage_file_id = int(request.POST['task_res_id'])
        stage_file_obj = OrderStageFile.objects.get(task_result_id=stage_file_id)
        
        new_cmnt_obj = OrderStageFileComment()
        new_cmnt_obj.order_stage_file = stage_file_obj
        new_cmnt_obj.comment = request.POST['comment']
        new_cmnt_obj.save()
        json_data = json.dumps({'message':'success'}, cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type='application/json')

@api_view(['POST'])
@csrf_exempt
def import_offer(request):
    if request.method == 'POST':
        status_dict={'1':'Not Started', '2':'In Progress', '3':'Completed'}
        if 'orderID' in request.POST:
            order_id = int(request.POST['orderID'])
            offer = Offer.objects.filter(orderID_id=order_id)
            if offer:
                offer[0].start_date = request.POST['start_date']
                offer[0].end_date = request.POST['end_date']
                offer[0].cost = request.POST['cost']
                offer[0].save()
            else:
                offer_obj = Offer()
                offer_obj.orderID_id = order_id
                offer_obj.start_date = request.POST['start_date']
                offer_obj.end_date = request.POST['end_date']
                offer_obj.cost = request.POST['cost']
                offer_obj.accepted = False
                offer_obj.save()

            #Get order stage files:
            stage_objects_list = request.POST.get('stage_objects', False)
            if stage_objects_list:
                stage_objects_list=eval(stage_objects_list)
                for f in stage_objects_list['data']:
                    exist_stage_obj = OrderStage.objects.filter(portal_stage_id=int(f['id']))
                    if exist_stage_obj:
                        exist_stage_obj[0].start_date=f['start_date']
                        exist_stage_obj[0].end_date=f['end_date']
                        exist_stage_obj[0].save()
                    else:    
                        status=str(f['status_id'])
                        order_stage_obj = OrderStage()
                        order_stage_obj.name = f['name']
                        order_stage_obj.orderID_id = order_id
                        order_stage_obj.portal_stage_id = int(f['id'])
                        order_stage_obj.start_date=f['start_date']
                        order_stage_obj.end_date=f['end_date']
                        order_stage_obj.completion_percentage=f['completion_percentage']
                        order_stage_obj.status=status_dict[status]
                        order_stage_obj.save()

        else:
            changed_status = request.POST.get('changed_status', False)
            if changed_status:
                pr_stage_id = int(request.POST['pr_stage_id'])
                exist_stage_obj = OrderStage.objects.get(portal_stage_id=pr_stage_id)
                exist_stage_obj.status=status_dict[changed_status]
                exist_stage_obj.completion_percentage=request.POST['completion_percentage']
                exist_stage_obj.save()

        json_data = json.dumps({'message':'success'}, cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type='application/json')


@api_view(['POST'])
@csrf_exempt
def import_stage_files(request):
        pr_stage_id = int(request.POST['pr_stage_id'])
        stage_obj = OrderStage.objects.filter(portal_stage_id=pr_stage_id)[0]

        stage_file_obj = OrderStageFile()
        stage_file_obj.name = request.POST['name']
        stage_file_obj.url = request.POST['file']
        stage_file_obj.file_size = int(request.POST['file_size'])
        stage_file_obj.task_result_id = int(request.POST['res_file_id'])
        stage_file_obj.upload_date = request.POST['upload_date']
        stage_file_obj.order_stage = stage_obj
        stage_file_obj.save()
        json_data = json.dumps({'message':'success'}, cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type='application/json')

@csrf_exempt
def change_order_status(request):
        order_id = int(request.POST['pr_order_id'])
        order_obj = Order.objects.get(id=order_id)

        order_obj.status_id = int(request.POST['status_id'])
        order_obj.completion_percentage = request.POST['completion_percentage']
        order_obj.save()
        json_data = json.dumps({'message':'success'}, cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type='application/json')