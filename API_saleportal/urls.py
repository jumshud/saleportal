from django.conf.urls import patterns, url
from API_saleportal import views


urlpatterns = patterns('',
    url(r'^message/',  views.create_message,  name='message'),
    url(r'^comment/',  views.import_comment,  name='comment'),
    url(r'^import_offer/',  views.import_offer,  name='import_offer'),
    url(r'^import_stage_files/',  views.import_stage_files,  name='import_stage_files'), 
    url(r'^change_order_status/',  views.change_order_status,  name='change_order_status'),
)