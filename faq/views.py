from django.views import generic as generic_views
from django.template import RequestContext

from models import Faq
from django.shortcuts import render,render_to_response

# Create your views here.


class FaqView(generic_views.ListView):
    """
    A simple faq view that exposes following context:

    * faq_obj
    
    """
    model = Faq
    template_name = 'faq/faq.html'   

    def get_context_data(self, **kwargs):
    	faq_obj = Faq.objects.all()
        context = super(FaqView, self).get_context_data(**kwargs)
        context['faq_obj'] = faq_obj
        return context
