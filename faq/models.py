from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Faq(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'))
    question_text = models.TextField(_('Question'), blank=True)
    answer_text = models.TextField(_('Answer'), blank=True)
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = _('Faq')
        verbose_name_plural = _('Faqs')
        ordering = ('-updated', )

    def __unicode__(self):
        return self.question_text


