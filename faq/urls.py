from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from . import views



urlpatterns = patterns('django.views.generic.date_based',
    url(r'^$',
        views.FaqView.as_view(), name='faq_index'),
)