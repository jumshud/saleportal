from django.contrib import admin

# Register your models here.
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.contrib import admin

from faq.models import Faq


class FaqAdmin(admin.ModelAdmin):
    """
        Admin for question
    """
    
    list_display = ('slug', 'title',  'question_text')
    search_fields = ['title', 'question_text', 'anwer_text']
    prepopulated_fields = {'slug': ('title',)}

    save_as = True
   

admin.site.register(Faq, FaqAdmin)


